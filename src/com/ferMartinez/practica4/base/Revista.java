package com.ferMartinez.practica4.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

public class Revista {

    private ObjectId id;
    private String nombre;
    private String autor;
    private String ventas;
    private LocalDate fecha_publicacion;

    @Override
    public String toString() {
        return "Revista{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", autor='" + autor + '\'' +
                ", ventas='" + ventas + '\'' +
                ", fecha_publicacion=" + fecha_publicacion +
                '}';
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getVentas() {
        return ventas;
    }

    public void setVentas(String ventas) {
        this.ventas = ventas;
    }

    public LocalDate getFecha_publicacion() {
        return fecha_publicacion;
    }

    public void setFecha_publicacion(LocalDate fecha_publicacion) {
        this.fecha_publicacion = fecha_publicacion;
    }

}
