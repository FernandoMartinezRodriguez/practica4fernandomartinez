package com.ferMartinez.practica4.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

public class Compra {

    private ObjectId id;
    private String comprador;
    private String vendedor;
    private String num_articulos;
    private String precio;

    @Override
    public String toString() {
        return "Compra{" +
                "id=" + id +
                ", comprador='" + comprador + '\'' +
                ", vendedor='" + vendedor + '\'' +
                ", num_articulos='" + num_articulos + '\'' +
                ", precio='" + precio + '\'' +
                '}';
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getComprador() {
        return comprador;
    }

    public void setComprador(String comprador) {
        this.comprador = comprador;
    }

    public String getVendedor() {
        return vendedor;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getNum_articulos() {
        return num_articulos;
    }

    public void setNum_articulos(String num_articulos) {
        this.num_articulos = num_articulos;
    }
}
