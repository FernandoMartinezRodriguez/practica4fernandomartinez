package com.ferMartinez.practica4.gui;

import com.ferMartinez.practica4.base.Compra;
import com.ferMartinez.practica4.base.Disco;
import com.ferMartinez.practica4.base.Revista;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

public class Controlador implements ActionListener, KeyListener, ListSelectionListener{

    Vista vista;
    Modelo modelo;

    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        inicializar();
    }


    private void inicializar() {
        addActionListeners(this);
        //addKeyListeners(this);
        addListSelectionListeners(this);
        modelo.conectar();
        listarCompras(modelo.getCompras());
        listarRevistas(modelo.getRevistas());
        listarDiscos(modelo.getDiscos());
    }

    private void addActionListeners(ActionListener listener){
        vista.btnEliminarCompra.addActionListener(listener);
        vista.btnEliminarDisco.addActionListener(listener);
        vista.btnEliminarRevista.addActionListener(listener);
        vista.btnModificarCompra.addActionListener(listener);
        vista.btnModificarDisco.addActionListener(listener);
        vista.btnModificarRevista.addActionListener(listener);
        vista.btnInsertarDisco.addActionListener(listener);
        vista.btnInsertarCompra.addActionListener(listener);
        vista.btnInsertarRevista.addActionListener(listener);
        vista.btnListarDiscos.addActionListener(listener);
        vista.btnListarRevistas.addActionListener(listener);
        vista.btnListarCompras.addActionListener(listener);
        vista.btnBuscarDiscos.addActionListener(listener);
        vista.btnBuscarCompras.addActionListener(listener);
        vista.btnBuscarRevistas.addActionListener(listener);
    }

    private void addListSelectionListeners(ListSelectionListener listener){
        vista.list1.addListSelectionListener(listener);
        vista.list2.addListSelectionListener(listener);
        vista.list3.addListSelectionListener(listener);
    }
    /*
    private void addKeyListeners(KeyListener listener){
        vista.txtBuscar.addKeyListener(listener);
    }

     */


    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        Disco unDisco;
        Revista unaRev;
        Compra unaComp;
        switch (comando){
            case "Nuevo":
                unDisco = new Disco();
                modificarDisco(unDisco);
                modelo.guardarDisco(unDisco);
                listarDiscos(modelo.getDiscos());
                break;
            case "Modificar":
                unDisco = (Disco) vista.list1.getSelectedValue();
                modificarDisco(unDisco);
                modelo.modificarDisco(unDisco);
                listarDiscos(modelo.getDiscos());
                break;
            case "Eliminar":
                unDisco = (Disco) vista.list1.getSelectedValue();
                modelo.borrarDisco(unDisco);
                listarDiscos(modelo.getDiscos());
                break;

            case "NuevoRe":
                unaRev = new Revista();
                modificarRevista(unaRev);
                modelo.guardarRevista(unaRev);
                listarRevistas(modelo.getRevistas());
                break;
            case "ModificarRe":
                unaRev = (Revista) vista.list2.getSelectedValue();
                modificarRevista(unaRev);
                modelo.modificarRevista(unaRev);
                listarRevistas(modelo.getRevistas());
                break;
            case "EliminarRe":
                unaRev = (Revista) vista.list2.getSelectedValue();
                modelo.borrarRevista(unaRev);
                listarRevistas(modelo.getRevistas());
                break;

            case "NuevoCompra":
                unaComp = new Compra();
                modificarCompra(unaComp);
                modelo.guardarCompra(unaComp);
                listarCompras(modelo.getCompras());
                break;
            case "ModificarCompra":
                unaComp = (Compra) vista.list3.getSelectedValue();
                modificarCompra(unaComp);
                modelo.modificarCompra(unaComp);
                listarCompras(modelo.getCompras());
                break;
            case "EliminarCompra":
                unaComp = (Compra) vista.list3.getSelectedValue();
                modelo.borrarCompra(unaComp);
                listarCompras(modelo.getCompras());
                break;

            case "ListarDiscos":
                listarDiscos(modelo.getDiscos());
                break;

            case "ListarRevistas":
                listarRevistas(modelo.getRevistas());
                break;

            case "ListarCompras":
                listarCompras(modelo.getCompras());
                break;

            case "BuscarDiscos":
                listarDiscos(modelo.getDisco(vista.txtBuscarDiscos.getText()));
                break;

            case "BuscarRevistas":
                listarRevistas(modelo.getRevista(vista.txtBuscarRevistas.getText()));
                break;

            case "BuscarCompras":
                listarCompras(modelo.getCompra(vista.txtBuscarCompras.getText()));
                break;
        }
    }


    private void listarCompras(List<Compra> listaCompra){
        vista.dlmCompra.clear();
        for (Compra Compra : listaCompra){
            vista.dlmCompra.addElement(Compra);
        }
    }

    private void listarRevistas(List<Revista> listaRe){
        vista.dlmRevista.clear();
        for (Revista Revista : listaRe){
            vista.dlmRevista.addElement(Revista);
        }
    }

    private void listarDiscos(List<Disco> listaDis){
        vista.dlmDisco.clear();
        for (Disco Disco : listaDis){
            vista.dlmDisco.addElement(Disco);
        }
    }

    private void modificarDisco(Disco unDisco) {
        unDisco.setNombre(vista.txtNombreDisco.getText());
        unDisco.setCantante(vista.txtCantanteDisco.getText());
        unDisco.setVentas(vista.txtVentasDisco.getText());
        unDisco.setFecha_salida(vista.fechaDisco.getDate());

    }

    private void modificarRevista(Revista unaRev) {
        unaRev.setNombre(vista.txtNombreRevista.getText());
        unaRev.setAutor(vista.txtAutorRevista.getText());
        unaRev.setVentas(vista.txtVentasRevista.getText());
        unaRev.setFecha_publicacion(vista.fechaRevista.getDate());
    }

    private void modificarCompra(Compra unaCompra) {
        unaCompra.setComprador(vista.txtComprador.getText());
        unaCompra.setVendedor(vista.txtVendedor.getText());
        unaCompra.setNum_articulos(vista.txtNumArticulos.getText());
        unaCompra.setPrecio(vista.txtPrecioCompra.getText());
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){
            if(e.getSource() == vista.list1) {
                Disco unDisco = (Disco) vista.list1.getSelectedValue();
                vista.txtNombreDisco.setText(unDisco.getNombre());
                vista.txtCantanteDisco.setText(unDisco.getCantante());
                vista.txtVentasDisco.setText(unDisco.getVentas());
                vista.fechaDisco.setDate(unDisco.getFecha_salida());
            }
            else if(e.getSource() == vista.list2){
                Revista unaRev = (Revista) vista.list2.getSelectedValue();
                vista.txtNombreRevista.setText(unaRev.getNombre());
                vista.txtAutorRevista.setText(unaRev.getAutor());
                vista.txtVentasRevista.setText(unaRev.getVentas());
                vista.fechaRevista.setDate(unaRev.getFecha_publicacion());
            }
            else{
                Compra unaCompra = (Compra) vista.list3.getSelectedValue();
                vista.txtComprador.setText(unaCompra.getComprador());
                vista.txtVendedor.setText(unaCompra.getVendedor());
                vista.txtNumArticulos.setText(unaCompra.getNum_articulos());
                vista.txtPrecioCompra.setText(String.valueOf(unaCompra.getPrecio()));
            }
        }
    }



    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }


}
