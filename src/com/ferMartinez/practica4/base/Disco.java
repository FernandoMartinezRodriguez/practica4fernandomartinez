package com.ferMartinez.practica4.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

public class Disco {

    private ObjectId id;
    private String nombre;
    private String cantante;
    private String ventas;
    private LocalDate fecha_salida;

    @Override
    public String toString() {
        return "Disco{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", cantante='" + cantante + '\'' +
                ", ventas='" + ventas + '\'' +
                ", fecha_salida=" + fecha_salida +
                '}';
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCantante() {
        return cantante;
    }

    public void setCantante(String cantante) {
        this.cantante = cantante;
    }

    public String getVentas() {
        return ventas;
    }

    public void setVentas(String ventas) {
        this.ventas = ventas;
    }

    public LocalDate getFecha_salida() {
        return fecha_salida;
    }

    public void setFecha_salida(LocalDate fecha_salida) {
        this.fecha_salida = fecha_salida;
    }
}
