package com.ferMartinez.practica4.gui;

import com.ferMartinez.practica4.base.Compra;
import com.ferMartinez.practica4.base.Disco;
import com.ferMartinez.practica4.base.Revista;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

public class Vista {
    JTabbedPane tabbedPane1;
    private JPanel panel1;
    JTextField txtNombreDisco;
    JTextField txtCantanteDisco;
    JTextField txtVentasDisco;
    JTextField txtNombreRevista;
    JTextField txtAutorRevista;
    JTextField txtVentasRevista;
    JButton btnInsertarDisco;
    JButton btnModificarDisco;
    JButton btnEliminarDisco;
    JList list1;
    JButton btnModificarRevista;
    JButton btnEliminarRevista;
    JTextField txtComprador;
    JTextField txtVendedor;
    JTextField txtPrecioCompra;
    JButton btnInsertarCompra;
    JButton btnModificarCompra;
    JButton btnEliminarCompra;
    JList list3;
    JList list2;
    DatePicker fechaDisco;
    DatePicker fechaRevista;
    JTextField txtNumArticulos;
    JButton btnInsertarRevista;
    JButton btnListarDiscos;
    JButton btnBuscarDiscos;
    JTextField txtBuscarDiscos;
    JButton btnListarRevistas;
    JButton btnBuscarRevistas;
    JTextField txtBuscarRevistas;
    JButton btnBuscarCompras;
    JButton btnListarCompras;
    JTextField txtBuscarCompras;


    DefaultListModel<Disco> dlmDisco;
    DefaultListModel<Compra> dlmCompra;
    DefaultListModel<Revista> dlmRevista;

    public Vista() {
        JFrame frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        inicializar();
    }

    private void inicializar(){
        dlmDisco = new DefaultListModel<>();
        list1.setModel(dlmDisco);
        dlmRevista = new DefaultListModel<>();
        list2.setModel(dlmRevista);
        dlmCompra = new DefaultListModel<>();
        list3.setModel(dlmCompra);

        fechaDisco.getComponentToggleCalendarButton().setText("Fecha");
        fechaRevista.getComponentToggleCalendarButton().setText("Fecha");
    }
}
