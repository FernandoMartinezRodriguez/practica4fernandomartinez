package com.ferMartinez.practica4;

import com.ferMartinez.practica4.gui.Controlador;
import com.ferMartinez.practica4.gui.Modelo;
import com.ferMartinez.practica4.gui.Vista;

public class Main {

    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
