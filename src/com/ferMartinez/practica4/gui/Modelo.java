package com.ferMartinez.practica4.gui;

import com.ferMartinez.practica4.base.Compra;
import com.ferMartinez.practica4.base.Disco;
import com.ferMartinez.practica4.base.Revista;
import com.ferMartinez.practica4.util.Util;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Modelo {

    private final static String COLECCION_REVISTAS = "Revistas";
    private final static String COLECCION_COMPRAS = "Compras";
    private final static String COLECCION_DISCOS = "Discos";
    private final static String DATABASE = "TiendaSegundaMano";

    private MongoClient client;
    private MongoDatabase baseDatos;
    private MongoCollection coleccionDiscos;
    private MongoCollection coleccionCompras;
    private MongoCollection coleccionRevistas;


    public void conectar() {
        client = new MongoClient();
        baseDatos = client.getDatabase(DATABASE);
        coleccionDiscos = baseDatos.getCollection(COLECCION_DISCOS);
        coleccionCompras = baseDatos.getCollection(COLECCION_COMPRAS);
        coleccionRevistas = baseDatos.getCollection(COLECCION_REVISTAS);
    }

    public void desconectar(){
        client.close();
    }


    public void guardarDisco(Disco unDisco) {
        coleccionDiscos.insertOne(discoToDocument(unDisco));
    }

    public void guardarCompra(Compra unaCompra) {
        coleccionCompras.insertOne(compraToDocument(unaCompra));
    }

    public void guardarRevista(Revista unaRevista) {
        coleccionRevistas.insertOne(revistaToDocument(unaRevista));
    }

    public List<Disco> getDiscos(){
        ArrayList<Disco> lista = new ArrayList<>();

        Iterator<Document> it = coleccionDiscos.find().iterator();
        while(it.hasNext()){
            lista.add(documentToDisco(it.next()));
        }
        return lista;
    }

    public List<Revista> getRevistas(){
        ArrayList<Revista> lista = new ArrayList<>();

        Iterator<Document> it = coleccionRevistas.find().iterator();
        while(it.hasNext()){
            lista.add(documentToRevista(it.next()));
        }
        return lista;
    }

    public List<Compra> getCompras(){
        ArrayList<Compra> lista = new ArrayList<>();

        Iterator<Document> it = coleccionCompras.find().iterator();
        while(it.hasNext()){
            lista.add(documentToCompra(it.next()));
        }
        return lista;
    }


    public List<Disco> getDisco(String text) {
        ArrayList<Disco> lista = new ArrayList<>();

        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("nombre", new Document("$regex", "/*"+text+"/*")));
        listaCriterios.add(new Document("cantante", new Document("$regex", "/*"+text+"/*")));
        query.append("$or", listaCriterios);

        Iterator<Document> iterator = coleccionDiscos.find(query).iterator();
        while(iterator.hasNext()){
            lista.add(documentToDisco(iterator.next()));
        }

        return lista;
    }

    public List<Compra> getCompra(String text) {
        ArrayList<Compra> lista = new ArrayList<>();

        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("comprador", new Document("$regex", "/*"+text+"/*")));
        listaCriterios.add(new Document("vendedor", new Document("$regex", "/*"+text+"/*")));
        query.append("$or", listaCriterios);

        Iterator<Document> iterator = coleccionCompras.find(query).iterator();
        while(iterator.hasNext()){
            lista.add(documentToCompra(iterator.next()));
        }

        return lista;
    }

    public List<Revista> getRevista(String text) {
        ArrayList<Revista> lista = new ArrayList<>();

        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("nombre", new Document("$regex", "/*"+text+"/*")));
        listaCriterios.add(new Document("autor", new Document("$regex", "/*"+text+"/*")));
        query.append("$or", listaCriterios);

        Iterator<Document> iterator = coleccionRevistas.find(query).iterator();
        while(iterator.hasNext()){
            lista.add(documentToRevista(iterator.next()));
        }

        return lista;
    }

    public Document discoToDocument(Disco unDisco){
        Document documento = new Document();
        documento.append("nombre", unDisco.getNombre());
        documento.append("cantante", unDisco.getCantante());
        documento.append("ventas", unDisco.getVentas());
        documento.append("fecha_salida", Util.formatearFecha(unDisco.getFecha_salida()));
        return documento;
    }
    public Document revistaToDocument(Revista unaRevista){
        Document documento = new Document();
        documento.append("nombre", unaRevista.getNombre());
        documento.append("autor", unaRevista.getAutor());
        documento.append("ventas", unaRevista.getVentas());
        documento.append("fecha_publicacion", Util.formatearFecha(unaRevista.getFecha_publicacion()));
        return documento;
    }
    public Document compraToDocument(Compra unaCompra){
        Document documento = new Document();
        documento.append("comprador", unaCompra.getComprador());
        documento.append("vendedor", unaCompra.getVendedor());
        documento.append("num_articulos", unaCompra.getNum_articulos());
        documento.append("precio", unaCompra.getPrecio());
        return documento;
    }

    public Disco documentToDisco(Document document){
        Disco unDisco = new Disco();
        unDisco.setId(document.getObjectId("_id"));
        unDisco.setNombre(document.getString("nombre"));
        unDisco.setCantante(document.getString("cantante"));
        unDisco.setVentas(document.getString("ventas"));
        unDisco.setFecha_salida(Util.parsearFecha(document.getString("fecha_salida")));
        return unDisco;
    }

    public Revista documentToRevista(Document document){
        Revista unRevista = new Revista();
        unRevista.setId(document.getObjectId("_id"));
        unRevista.setNombre(document.getString("nombre"));
        unRevista.setAutor(document.getString("autor"));
        unRevista.setVentas(document.getString("ventas"));
        unRevista.setFecha_publicacion(Util.parsearFecha(document.getString("fecha_publicacion")));
        return unRevista;
    }

    public Compra documentToCompra(Document document){
        Compra unaCompra = new Compra();
        unaCompra.setId(document.getObjectId("_id"));
        unaCompra.setComprador(document.getString("comprador"));
        unaCompra.setVendedor(document.getString("vendedor"));
        unaCompra.setNum_articulos(document.getString("num_articulos"));
        unaCompra.setPrecio(document.getString("precio"));
        return unaCompra;
    }

    public void modificarDisco(Disco unDisco) {
        coleccionDiscos.replaceOne(new Document("_id", unDisco.getId()), discoToDocument(unDisco));
    }

    public void borrarDisco(Disco unDisco) {

        coleccionDiscos.deleteOne(discoToDocument(unDisco));
    }

    public void modificarRevista(Revista unaRevista) {
        coleccionRevistas.replaceOne(new Document("_id", unaRevista.getId()), revistaToDocument(unaRevista));
    }

    public void borrarRevista(Revista unaRevista) {
        coleccionRevistas.deleteOne(revistaToDocument(unaRevista));
    }

    public void modificarCompra(Compra unaCompra) {
        coleccionCompras.replaceOne(new Document("_id", unaCompra.getId()), compraToDocument(unaCompra));
    }

    public void borrarCompra(Compra unaCompra) {
        coleccionCompras.deleteOne(compraToDocument(unaCompra));
    }
}
